//
// Created by Alex Vishnevskiy on 05.05.2020.
//

#pragma once
#include <vector>

class IComputor {
public:
    virtual void printReduced() = 0;
    virtual void compute(std::vector<std::string>) = 0;
    virtual void findPolynomial() = 0;
    virtual void reduceExpr() = 0;
};