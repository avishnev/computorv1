#include <vector>
#include <map>
#include "IComputor.h"

class Values {
public:
    Values() = default;
    virtual ~Values() = default;
    typedef std::map<std::string, float> DataPair;

protected:
    float _a {};
    float _b {};
    float _c {};
    DataPair _map;

};

class ComputorV1 final : public Values, public IComputor
{
public:
    void printResult();
    void compute(std::vector<std::string>) override;

private:
    void printReduced() override;
    void findPolynomial() override;
    void reduceExpr() override;
    void findABC();

private:
    int32_t     m_polynomial{};
    long double m_discriminant{};
    std::vector<std::string> m_data;
};
