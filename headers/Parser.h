#include <iostream>
#include <list>
#include <vector>

class Parser final
{

public:
    explicit Parser(const char *);
    ~Parser() = default;

    [[nodiscard]]std::vector<std::string> getParsedData() const;

#ifdef LIVE
    static bool isLiveMode(int mode) {
        return (mode == 1);
    };
#endif
private:
    void m_analyzeData(const char *);
    std::vector<std::string> m_computeUnit;
};
