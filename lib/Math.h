#pragma once
namespace avishnev {

class Math {
public:
    Math() = default;

    ~Math() = default;

    template<typename T>
    [[nodiscard]] static T pow(T v1, int v2) {
        T res = 1;
        if (v2 > 0) {
            for (int i = 0; i < v2; ++i)
                res *= v1;
        } else {
            for (int i = v2; i < 0; i++)
                res *= (1 / v1);
        }
        return res;
    }

#ifdef IMPLEMENTED
        template<typename T>
        [[nodiscard]] static double sqrt(double, double);
#endif

    template<typename T>
    [[nodiscard]] static T abs(T val) {
        return (val < 0) ? (val * -1.0) : val;
    }
    template<typename T>
    [[nodiscard]] static T div(T _div, T divider, int accur) {
        T quotient = 0.0;
        int sign = 1;

        if (_div * divider < 0)
            sign = -1;
        _div = Math::abs<T>(_div);
        divider = Math::abs<T>(divider);

        if (divider == 0)
            throw(std::exception());

        while (_div > divider)
        {
            _div = Math::sub<T>(_div, divider);
            quotient += 1;
        }

        if (accur > 0 && _div != 0)
            quotient += Math::div<T>(_div * 10, divider, sub(accur ,1)) * 0.1;
        return (quotient * sign);
    }

    template<typename T>
    [[nodiscard]] static T sub(T v1, T v2) {
        return (v1 + (-1 * v2));
    }

    private:
};
}