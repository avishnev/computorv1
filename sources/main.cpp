
#include "ComputorV1.h"
#include "Parser.h"

int main(int argc, const char **argv)
{
    auto cpu = std::make_unique<ComputorV1>();

    if (argc == 2) {
        auto parser = std::make_unique<Parser>(argv[1]);
        try {
            cpu->compute(parser->getParsedData());
        }
        catch (std::exception &e) {
            std::cerr << e.what() << std::endl;
        }
        cpu->printResult();
    }
    else {
        std::cerr << "Wrong input parametrs" << std::endl;
        exit(EXIT_FAILURE);
    }
    return (EXIT_SUCCESS);
}