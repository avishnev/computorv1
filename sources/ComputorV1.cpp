#include "ComputorV1.h"
#include <iostream>
#include <utility>
#include "Math.h"
#include <string>
#include <sstream>
#include <iomanip>

using std::cout;
using std::endl;
using am = avishnev::Math;
void ComputorV1::printResult()
{
    std::stringstream ss;
    if (m_polynomial == 0) {
        if (_c == 0.0)
            ss << "The solution is:" << endl << ((_c == 0) ? "X is an element of all Real numbers" :
                "X is unresolvable") << endl;
    }
    else if (m_polynomial <= 2) {
        if (m_discriminant > 0) {
            ss << "Discriminant is strictly positive, the two solutions are:" << endl;
            ss << "The solution is:" << endl;
        }
        else if (m_discriminant == 0)
            ss << "Discriminant is strictly positive, the two solutions are:" << endl;
    } else
        ss << "The polynomial greater than 2, it isn't ComputorV1 V2, so ..." << endl;
    cout << avishnev::Math::abs<int>(10) << endl;

    cout << ss.str();
}

void ComputorV1::compute(std::vector<std::string> data)
{
    m_data = std::move(data);
    reduceExpr();
    printReduced();
    findPolynomial();
    findABC();

    cout << "DELETE THIS:" <<(m_discriminant = am::sub<float>(
            am::pow<float>(_b, 2),
            (4 * _a * _c)))
            << endl;
}

void ComputorV1::findPolynomial()
{
    for (const auto& v: m_data)
    {
        if (v.find("X^") != std::string::npos) {
            auto tmp = std::stol(v.c_str() + ((v.c_str()[0]== '-') ? 3 : 2), nullptr, 10);
            m_polynomial = (m_polynomial < tmp) ? tmp : m_polynomial;
        }
    }
}

void ComputorV1::reduceExpr()
{
//    for (auto& v: m_data) {
//        if (v == "X^0") {
//            v.clear();
//            v += "1";
//        }
//    }

    bool equal = false;

    for (auto& v: m_data) {

        if (equal && (v != "*" && v != "/" && v != "-" && v != "+" && v.find("X^") == std::string::npos))
            v.insert(0, std::string("-"));
        if (v == "=")
            equal = true;
        cout << v << " ";
    }

    for (auto i = m_data.begin(); i != m_data.end(); i++) {
        if ((*i == "*" || *i == "/") && *(i + 1) == "1") {
            (*(i)).clear();
            (*(i + 1)).clear();
        } else if ((*i).find("X^") != std::string::npos) {
            auto it = _map.find(*i);
            if (it != _map.end())
                it->second += std::stof((*(i - 1) == "*") ? *(i - 2) : "1");
            else {
                _map[*i] = std::stof((*(i - 1) == "*") ? *(i - 2) : "1");
                if ((i - 2) != m_data.begin())
                   _map[*i] = *(i - 3) == "-" ? _map[*i] * -1 : _map[*i];
            }
        }
    }
    (void)std::remove_if(m_data.begin(), m_data.end(), [](std::string &a){ return a.empty();});
    cout << endl;
    for (const auto &r: _map)
        cout << r.first << " | " << r.second << endl;
    cout << endl;
}

void ComputorV1::findABC()
{
    _a = _map.find("X^2") != _map.end() ? _map["X^2"] : 0;
    _b = _map.find("X^1") != _map.end() ? _map["X^1"] : 0;
    _c = _map.find("X^0") != _map.end() ? _map["X^0"] : 0;
}

void ComputorV1::printReduced()
{
    cout << "Redused form: ";

    for (auto it = _map.rbegin(); it != _map.rend() ; ++it) {
        if (it->second != 0.0) {
            if (it != _map.rbegin()) {
                cout << (it->second < 0.0 ? " - " : " + ");
                cout << std::to_string(am::abs<float>(it->second)) << it->first;
            }
            else
                cout << std::to_string(it->second) << it->first;
        }
    }
    cout << " = 0;\n";
}
