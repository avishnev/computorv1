#include "Parser.h"
#include <regex>

#define ERROR_EXIT(x) {std::cerr << x << std::endl; exit(EXIT_FAILURE);}

std::vector<std::string> Parser::getParsedData() const
{
    return std::ref(m_computeUnit);
}

void Parser::m_analyzeData(const char *data)
{
    std::string d(data);

    if (d.empty())
        ERROR_EXIT("Empty input data")
    if (!std::regex_match(d, std::regex("[0-9 .^*+X=\\-]+")))
        ERROR_EXIT("Non valid input data")
    if (d.find('=') == std::string::npos)
        ERROR_EXIT("Parse error: No '=' has found")

    while (d.find(' ') != std::string::npos) {
        m_computeUnit.push_back(d.substr(0, d.find(' ')));
        d.erase(0, d.find(' ') + 1);
    }
    m_computeUnit.push_back(d);
}

Parser::Parser(const char *expression) {
    std::cout << "string to parse: " << expression << std::endl;
    m_analyzeData(expression);
}
